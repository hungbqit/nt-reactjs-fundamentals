import React from 'react';
import './App.css';
import { NotificationSettings } from './components/notification-settings';
import { Cards } from './components/cards';

function App() {
  return (
    <div className="App">
      <h2 className="mt-5">Rating components</h2>
      <div className='row'>
        <div className='col-md-4'>
          <NotificationSettings />
        </div>
      </div>
      <h2 className="mt-5">Card components</h2>
      <div className='row'>
        <div className='col'>
          <Cards />
        </div>
      </div>
    </div>
  );
}

export default App;
