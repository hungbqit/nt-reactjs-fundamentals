import React, { Component } from 'react';
import { Card } from 'antd';
import { PushpinOutlined, CheckOutlined } from '@ant-design/icons';

enum CardType {
    Distance,
    Price
}

interface CardData {
    id: string,
    type: CardType,
    value: number,
    description?: string,
    icon: JSX.Element,
    iconBackgroundColor: string
}

export class CardItem extends Component<{ data: CardData }, {}>{
    render() {
        const { data } = this.props;
        return (
            <Card className='d-inline-block'>
                <span className='rounded-icon' style={{ backgroundColor: data.iconBackgroundColor }}>{data.icon}</span>
                <h2 className={'mt-2 mb-1 d-flex' + (data.type === CardType.Distance ? ' flex-row-reverse' : '')}>
                    <span className={data.type === CardType.Distance ? 'flex-grow-1 ml-1' : ''}>
                        {data.type === CardType.Distance ? 'mi' : '$'}
                    </span>
                    <span>{data.value.toLocaleString('en')}</span>
                </h2>
                <p className='m-0 text-muted'>{data.description}</p>
            </Card>
        );
    }
}

const CardMemo = React.memo(CardItem);

export class Cards extends Component<{}, { cards: Array<CardData> }>{
    state = {
        cards: [
            {
                id: 'distance',
                type: CardType.Distance,
                value: 158.3,
                description: 'Distance driven',
                icon: <PushpinOutlined style={{ fontSize: '18px', color: '#00acc1' }} />,
                iconBackgroundColor: '#e0f7fa'
            },
            {
                id: 'vehicles',
                type: CardType.Price,
                value: 1428,
                description: 'Vehicles on track',
                icon: <CheckOutlined style={{ fontSize: '18px', color: '#43a047' }} />,
                iconBackgroundColor: '#e8f5e9'
            }
        ]
    }

    render() {
        const { cards } = this.state;
        return (
            <div className='cards-container'>{
                cards.map((item: CardData) => {
                    return (
                        <CardMemo key={item.id} data={item} />
                    );
                })
            }</div>
        );
    }
}