import React, { Component } from 'react';
import { Switch } from 'antd';

interface NotificationData {
    id: string,
    title: string,
    description?: string,
    status: boolean
}

export class NotificationItem extends Component<{
    data: NotificationData,
    onChange: (checked: boolean, data: NotificationData) => void
}, {}>{
    handleSwitchChange = (checked: boolean) => {
        this.props.onChange(checked, this.props.data);
    }

    render() {
        const { data } = this.props;
        return (
            <li className="list-group-item d-flex align-items-center justify-content-center">
                <div className='flex-grow-1'>
                    <h5 className='m-0'>{data.title}</h5>
                    <p className='m-0'>{data.description}</p>
                </div>
                <Switch checked={data.status} onChange={(checked) => this.handleSwitchChange(checked)} />
            </li>
        );
    }
}

const NotificationMemo = React.memo(NotificationItem);

export class NotificationSettings extends Component<{}, { notifications: Array<NotificationData> }>{
    state = {
        notifications: [
            {
                id: 'email',
                title: 'Email Notification',
                description: 'Lorem ipsum dolor sit amet',
                status: true
            },
            {
                id: 'push',
                title: 'Push Notification',
                description: 'Lorem ipsum dolor sit amet',
                status: false
            },
            {
                id: 'monthly-reports',
                title: 'Monthly Reports',
                description: 'Lorem ipsum dolor sit amet',
                status: false
            },
            {
                id: 'quater-reports',
                title: 'Quater Reports',
                description: 'Lorem ipsum dolor sit amet',
                status: false
            }
        ]
    }

    handleSwitchChange = (checked: boolean, data: NotificationData) => {
        const list = this.state.notifications.map(item => {
            if (item.id === data.id) {
                return { ...item, status: checked };
            }
            return item;
        });
        this.setState({ notifications: list });
    };

    render() {
        const { notifications } = this.state;
        return (
            <div className="list-group">{
                notifications.map((item: NotificationData) => {
                    return (
                        <NotificationMemo key={item.id} data={item} onChange={this.handleSwitchChange} />
                    );
                })
            }</div>
        );
    }
}