import React, { Component } from "react";
import { CardList } from "../card/card-list";
import { RequestList } from "../request/request-list";
import { ProfileInfo } from "../profile/profile-info";

export class Dashboard extends Component {
    render() {
        return (
            <div className="row mt-5">
                <div className='col-9'>
                    <div className='row'>
                        <div className='col'>
                            <CardList />
                        </div>
                    </div>
                    <div className='row mt-5'>
                        <div className='col'>
                            <RequestList />
                        </div>
                    </div>
                </div>
                <div className='col-3'>
                    <ProfileInfo />
                </div>
            </div>
        );
    }
}