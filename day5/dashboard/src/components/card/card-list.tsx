import React, { useReducer, useEffect, Fragment } from 'react';

import { CardData } from './card-data';
import { CardMemo } from './card-item';
import { fetchData } from '../../utils/fetch-data';

interface CardListState {
    items: Array<CardData>,
    loading: boolean
}

interface ActionTypes {
    readonly type: 'GET_LIST' | 'GET_LIST_SUCCESS' | 'GET_LIST_FAILED',
    payload?: Array<CardData>
}

function reducer(state: CardListState, action: ActionTypes) {
    switch (action.type) {
        case 'GET_LIST':
            return { ...state, loading: true };
        case 'GET_LIST_SUCCESS':
            return { ...state, loading: false, items: action.payload! };
        case 'GET_LIST_FAILED':
            return { ...state, loading: false };
        default:
            throw new Error('Unknown action!!!');
    }
}

export function CardList() {
    const endpoint = 'http://localhost:3004/cards';
    const [state, dispatch] = useReducer(reducer, { items: [], loading: false });
    const { items, loading } = state;

    useEffect(() => {
        dispatch({ type: 'GET_LIST' });
        (async () => {
            try {
                const response = await fetchData(endpoint);
                const data = await response.json() as Array<CardData>;
                dispatch({ type: 'GET_LIST_SUCCESS', payload: data });
            } catch (error) {
                console.log(error);
                dispatch({ type: 'GET_LIST_FAILED' });
            }
        })();
    }, []);

    return (
        <div className='cards-container'>
            {loading ? <span>loading...</span> : <Fragment>
                {items.map((item: CardData) => <CardMemo key={item.id} data={item} />)}
            </Fragment>}
        </div>
    );
}