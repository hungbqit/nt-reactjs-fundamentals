import React, { Component } from 'react';
import { Card } from 'antd';

import { CardData, CardType } from './card-data';
import { MailOutlined, MessageOutlined, CalendarOutlined } from '@ant-design/icons';

export class CardItem extends Component<{ data: CardData }, {}>{
    render() {
        const { data } = this.props;
        switch (data.type) {
            case CardType.Tickets:
                data.icon = <MailOutlined style={{ fontSize: '18px', color: '#8E24AA' }} />;
                data.iconBackgroundColor = '#F3E5F5';
                break;
            case CardType.Average:
                data.icon = <MessageOutlined style={{ fontSize: '18px', color: '#C0CA33' }} />;
                data.iconBackgroundColor = '#F9FBE7';
                break;
            case CardType.Median:
                data.icon = <CalendarOutlined style={{ fontSize: '18px', color: '#00ACC1' }} />;
                data.iconBackgroundColor = '#E0F7FA';
                break;
        }
        return (
            <Card className='d-inline-block shadow bg-white rounded' bordered={false}>
                <div className='d-flex'>
                    <span className='rounded-icon' style={{ backgroundColor: data.iconBackgroundColor }}>{data.icon}</span>
                    <div className='ml-3'>
                        <h2 className='d-flex mb-1'>
                            <span>{data.value.toLocaleString('en')}</span>
                            <span className={data.type === CardType.Average ? 'flex-grow-1 ml-1' : ''}>
                                {data.type === CardType.Average ? 'm' : ''}
                            </span>
                        </h2>
                        <p className='m-0 text-muted'>{data.description}</p>
                    </div>
                </div>
            </Card>
        );
    }
}

export const CardMemo = React.memo(CardItem);