export enum CardType {
    Tickets = 1,
    Average = 2,
    Median = 3
}

export interface CardData {
    id: string,
    type: CardType,
    value: number,
    description?: string,
    icon: JSX.Element,
    iconBackgroundColor: string
}