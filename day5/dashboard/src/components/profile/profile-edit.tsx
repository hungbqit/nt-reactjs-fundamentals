import React, { Fragment, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Button, Form, Input, Select, Upload } from 'antd';
import { RcFile } from 'antd/lib/upload';
import { ArrowLeftOutlined, UploadOutlined } from '@ant-design/icons';
import { ProfileData, Role } from './profile-data';
import { fetchData } from '../../utils/fetch-data';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export function EditProfile() {
    const history = useHistory();
    const endpoint = 'http://localhost:3004/profile';
    const [loading, setLoading] = useState(false);
    const [formData, setFormData] = useState({
        name: '',
        title: '',
        email: '',
        phone: '',
        twitter: '',
        location: '',
        bio: '',
        roles: ['']
    });
    const [avatar, setAvatar] = useState('');

    useEffect(() => {
        setLoading(true);
        (async () => {
            try {
                const response = await fetchData(`${endpoint}`);
                const data = await response.json() as ProfileData;
                setFormData({
                    name: data.name,
                    title: data.title,
                    email: data.email,
                    phone: data.phone,
                    twitter: data.twitter,
                    location: data.location,
                    bio: data.bio,
                    roles: data.roles
                });
                setAvatar(data.avatar);
            } catch (error) {
                toast.error(error);
            }
            finally {
                setLoading(false);
            }
        })();
    }, []);

    const onAvatarChange = (file: RcFile, fileList: RcFile[]) => {
        const validTypes = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!validTypes) {
            toast.error('You can only upload JPG/PNG file!');
            fileList.pop();
        }
        const validSize = file.size / 1024 / 1024 < 2;
        if (!validSize) {
            toast.error('Image must smaller than 2MB!');
            fileList.pop();
        }

        const reader = new FileReader();
        reader.onload = () => setAvatar(reader.result as string);
        reader.readAsDataURL(file);

        fileList.pop();
        return false;
    };

    const onFormChange = (values: any) => setFormData(values);

    const submit = async (values: any) => {
        const options = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ ...values, avatar: avatar })
        };
        const response = await fetch(`${endpoint}`, options);
        if (response.status < 200 || response.status >= 300) {
            toast.error(response.statusText);
        } else {
            toast.info(response.statusText, { onClose: () => history.goBack() });
        }
    };

    return (
        <Fragment>
            <ToastContainer />
            <div className='mt-5'>
                <Button type='primary' icon={<ArrowLeftOutlined />} size='large' href='/'>Back</Button>
                <h2 className='my-3'>Edit Profile</h2>
            </div>
            <div className='mt-5 bg-white px-5 py-3 shadow-sm rounded'>
                {loading ? <span>loading...</span> :
                    <Form labelCol={{ span: 10 }} wrapperCol={{ span: 10 }} layout='vertical' size='middle' className='profile-edit-form'
                        initialValues={formData}
                        onValuesChange={onFormChange}
                        onFinish={submit}>
                        <Form.Item label='Avatar'>
                            <div className='d-flex align-items-center'>
                                <div>
                                    <Upload action='' className='w-auto' multiple={false} beforeUpload={onAvatarChange}>
                                        <Button><UploadOutlined /> Change Avatar</Button>
                                    </Upload>
                                </div>
                                <img src={avatar} className='avatar mb-2 ml-auto' alt='' />
                            </div>
                        </Form.Item>
                        <Form.Item name='name' label='Full Name' required={true}>
                            <Input value={formData?.name} />
                        </Form.Item>
                        <Form.Item name='title' label='Job Title' required={true}>
                            <Input value={formData?.title} />
                        </Form.Item>
                        <Form.Item name='roles' label='Roles' required={true} rules={[{ type: 'array' }]}>
                            <Select mode="multiple" placeholder="Please select roles" value={formData?.roles}>
                                {
                                    Object.keys(Role).filter(key => isNaN(Number(key))).map(key => {
                                        return (<Select.Option key={key} value={key}>{key}</Select.Option>);
                                    })
                                }
                            </Select>
                        </Form.Item>
                        <Form.Item name='email' label='Email Address' required={true} rules={[{ type: 'email' }]}>
                            <Input value={formData?.email} />
                        </Form.Item>
                        <Form.Item name='phone' label='Phone Number' required={true}>
                            <Input value={formData?.phone} />
                        </Form.Item>
                        <Form.Item name='twitter' label='Twitter'>
                            <Input value={formData?.twitter} />
                        </Form.Item>
                        <Form.Item name='location' label='Location'>
                            <Input value={formData?.location} />
                        </Form.Item>
                        <Form.Item name='bio' label='Biography'>
                            <Input.TextArea value={formData?.bio} autoSize={true} />
                        </Form.Item>
                        <Form.Item>
                            <Button type='primary' htmlType='submit'>Save</Button>
                            <Button htmlType='button' href='/' className='ml-2'>Cancel</Button>
                        </Form.Item>
                    </Form>}
            </div>
        </Fragment >
    );
}