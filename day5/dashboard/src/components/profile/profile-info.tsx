import React, { useReducer, useEffect, Fragment } from 'react';
import { ProfileData } from './profile-data';
import { fetchData } from '../../utils/fetch-data';
import { Button } from 'antd';


interface ProfileInfoState {
    data: ProfileData | null,
    loading: boolean
}

interface ActionTypes {
    readonly type: 'GET_PROFILE' | 'GET_PROFILE_SUCCESS' | 'GET_PROFILE_FAILED',
    payload?: ProfileData
}

function reducer(state: ProfileInfoState, action: ActionTypes) {
    switch (action.type) {
        case 'GET_PROFILE':
            return { ...state, loading: true };
        case 'GET_PROFILE_SUCCESS':
            return { ...state, loading: false, data: action.payload! };
        case 'GET_PROFILE_FAILED':
            return { ...state, loading: false };
        default:
            throw new Error('Unknown action!!!');
    }
}

export function ProfileInfo() {
    const endpoint = 'http://localhost:3004/profile';
    const [state, dispatch] = useReducer(reducer, { data: null, loading: false });
    const { data, loading } = state;

    useEffect(() => {
        dispatch({ type: 'GET_PROFILE' });
        (async () => {
            try {
                const response = await fetchData(endpoint);
                const data = await response.json() as ProfileData;
                dispatch({ type: 'GET_PROFILE_SUCCESS', payload: data });
            } catch (error) {
                console.log(error);
                dispatch({ type: 'GET_PROFILE_FAILED' });
            }
        })();
    }, []);

    return (
        <div className='profile-info-container px-3 py-4 bg-white shadow-sm rounded'>
            {loading ? <span>loading...</span> : <Fragment>
                <div className='text-center'>
                    <img src={data?.avatar} className="avatar mb-2" alt={data?.name} />
                    <h4 className='mb-0'>{data?.name}</h4>
                    <p>{data?.title}</p>
                    <div className='d-flex justify-content-between'>
                        <Button type="primary" href='/profile'>Edit profile</Button>
                        <Button>Change Status</Button>
                    </div>
                </div>
                <div className='profile-info border-top mt-4 pt-4'>
                    <label className=''>Role</label>
                    <p>{data?.roles.join(', ')}</p>
                    <label className=''>Email</label>
                    <p>{data?.email}</p>
                    <label className=''>Phone</label>
                    <p>{data?.phone}</p>
                    <label className=''>Twitter</label>
                    <p>{data?.twitter}</p>
                    <label className=''>Location</label>
                    <p>{data?.location}</p>
                    <label className=''>Bio</label>
                    <p>{data?.bio}</p>
                </div>
            </Fragment>}
        </div>
    );
}