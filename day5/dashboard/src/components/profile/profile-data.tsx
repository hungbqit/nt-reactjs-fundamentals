export enum Role {
    Administrator,
    Moderator,
    User
}

export interface ProfileData {
    name: string,
    avatar: string,
    title: string,
    email: string,
    phone: string,
    twitter: string,
    location: string,
    bio: string,
    roles: Array<string>
}