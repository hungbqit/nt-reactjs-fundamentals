export enum RequestStatus {
    Active = 1,
    Resolved = 2,
    Paused = 0,
    Cancelled = -1
}

export interface RequestData {
    id: number,
    subject: string,
    requestedDate: string,
    latestUpdate: string,
    status: RequestStatus
}