import React, { useReducer, useEffect, Fragment } from 'react';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';
import { Table } from 'antd';

import { RequestData, RequestStatus } from './request-data';
import { fetchData } from '../../utils/fetch-data';

interface RequestListState {
    items: Array<RequestData>,
    loading: boolean
}

interface ActionTypes {
    readonly type: 'GET_LIST' | 'GET_LIST_SUCCESS' | 'GET_LIST_FAILED',
    payload?: Array<RequestData>
}

function reducer(state: RequestListState, action: ActionTypes) {
    switch (action.type) {
        case 'GET_LIST':
            return { ...state, loading: true };
        case 'GET_LIST_SUCCESS':
            return { ...state, loading: false, items: action.payload! };
        case 'GET_LIST_FAILED':
            return { ...state, loading: false };
        default:
            throw new Error('Unknown action!!!');
    }
}

export function RequestList() {
    const endpoint = 'http://localhost:3004/requests';
    const [state, dispatch] = useReducer(reducer, { items: [], loading: false });
    const { items, loading } = state;
    const columns = [
        {
            title: 'Id',
            dataIndex: 'id',
            key: 'id',
            render: (value: number, row: any, index: number) => `#${value}`,
            className: 'bolder'
        },
        {
            title: 'Subject',
            dataIndex: 'subject',
            key: 'subject'
        },
        {
            title: 'Requested Date',
            dataIndex: 'requestedDate',
            key: 'requestedDate',
            render: (value: Date, row: any, index: number) => {
                return <Moment format='MM/DD/YYYY'>{value}</Moment>;
            },
            className: 'text-center'
        },
        {
            title: 'Latest Update',
            dataIndex: 'latestUpdate',
            key: 'latestUpdate',
            render: (value: Date, row: any, index: number) => {
                return <Moment format='MM/DD/YYYY'>{value}</Moment>;
            },
            className: 'text-center'
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
            render: (value: number, row: any, index: number) => { return RequestStatus[value]; },
            className: 'text-center'
        },
        {
            title: '',
            key: 'action',
            render: (text: string, row: any) => (
                <Link to={`/requests/${row.id}`}>Open</Link>
            ),
            className: 'bolder'
        }
    ];

    useEffect(() => {
        dispatch({ type: 'GET_LIST' });
        (async () => {
            try {
                const response = await fetchData(endpoint);
                const data = await response.json() as Array<RequestData>;
                dispatch({ type: 'GET_LIST_SUCCESS', payload: data });
            } catch (error) {
                console.log(error);
                dispatch({ type: 'GET_LIST_FAILED' });
            }
        })();
    }, []);

    return (
        <Fragment>
            {loading ? <span>loading...</span> : <Table dataSource={items} columns={columns} rowKey='id' pagination={{ position: ['bottomLeft'] }} />}
        </Fragment>
    );
}