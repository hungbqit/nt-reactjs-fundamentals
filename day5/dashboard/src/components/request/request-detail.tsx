import React, { Fragment, useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Button, Form, Input, Select, DatePicker } from 'antd';
import { ArrowLeftOutlined } from '@ant-design/icons';
import { RequestStatus, RequestData } from './request-data';
import { fetchData } from '../../utils/fetch-data';
import moment from 'moment';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export function RequestDetail() {
    const history = useHistory();
    const endpoint = 'http://localhost:3004/requests';
    const [loading, setLoading] = useState(false);
    const [formData, setFormData] = useState({
        id: 0,
        subject: '',
        requestedDate: moment(),
        latestUpdate: moment(),
        status: 0
    });

    const { id } = useParams();
    useEffect(() => {
        setLoading(true);
        (async () => {
            try {
                const response = await fetchData(`${endpoint}/${id}`);
                const data = await response.json() as RequestData;
                setFormData({
                    id: data.id,
                    subject: data.subject,
                    requestedDate: moment(data.requestedDate),
                    latestUpdate: moment(data.latestUpdate),
                    status: data.status
                });
            } catch (error) {
                toast.error(error);
            }
            finally {
                setLoading(false);
            }
        })();
    }, [id]);

    const onFormChange = (values: any) => setFormData(values);

    const submit = async (values: any) => {
        const options = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(values)
        };
        const response = await fetch(`${endpoint}/${id}`, options);
        if (response.status < 200 || response.status >= 300) {
            toast.error(response.statusText);
        } else {
            toast.info(response.statusText, { onClose: () => history.goBack() });
        }
    };

    return (
        <Fragment>
            <ToastContainer />
            <div className='mt-5'>
                <Button type='primary' icon={<ArrowLeftOutlined />} size='large' href='/'>Back</Button>
                <h2 className='my-3'>Request Detail <span className='text-info'>{id}</span></h2>
            </div>
            <div className='mt-5 bg-white px-5 py-3 shadow-sm rounded'>
                {loading ? <span>loading...</span> :
                    <Form labelCol={{ span: 10 }} wrapperCol={{ span: 10 }} layout='vertical' size='middle'
                        initialValues={formData}
                        onValuesChange={onFormChange}
                        onFinish={submit}>
                        <Form.Item name='id' label='ID'>
                            <Input value={formData?.id} />
                        </Form.Item>
                        <Form.Item name='subject' label='Subject'>
                            <Select value={formData?.subject}>
                                <Select.Option value='Refund request'>Refund request</Select.Option>
                                <Select.Option value='Return request'>Return request</Select.Option>
                            </Select>
                        </Form.Item>
                        <Form.Item name='requestedDate' label='Requested Date'>
                            <DatePicker value={formData?.requestedDate} className='w-100' />
                        </Form.Item>
                        <Form.Item name='latestUpdate' label='Latest Update'>
                            <DatePicker value={formData?.latestUpdate} className='w-100' />
                        </Form.Item>
                        <Form.Item name='status' label='Status'>
                            <Select value={Number(formData?.status)}>
                                {
                                    Object.keys(RequestStatus).filter(key => !isNaN(Number(key))).map(key => {
                                        return (<Select.Option key={key} value={Number(key)}>{RequestStatus[Number(key)]}</Select.Option>);
                                    })
                                }
                            </Select>
                        </Form.Item>
                        <Form.Item>
                            <Button type='primary' htmlType='submit'>Save</Button>
                            <Button htmlType='button' href='/' className='ml-2'>Cancel</Button>
                        </Form.Item>
                    </Form>}
            </div>
        </Fragment >
    );
}