import React, { Component } from "react";
import { RequestList } from "../request/request-list";
import { Button } from "antd";
import { ArrowLeftOutlined } from '@ant-design/icons';

export class RequestPage extends Component {
    render() {
        return (
            <div className="mt-5" >
                <Button type="primary" icon={<ArrowLeftOutlined />} size='large' href='/'>Back</Button>
                <h2 className="my-3">Requests</h2>
                <RequestList />
            </div>
        );
    }
}