export async function fetchData(input: RequestInfo, init?: RequestInit | undefined) {
    const response = await fetch(input);
    if (response.status < 200 || response.status >= 300) {
        throw response;
    }
    return response;
}