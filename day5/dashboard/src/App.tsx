import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Dashboard } from './components/common/dashboard';
import { RequestPage } from './components/request/request-page';

import './App.css';
import { RequestDetail } from './components/request/request-detail';
import { EditProfile } from './components/profile/profile-edit';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Dashboard} />
          <Route path="/dashboard" exact component={Dashboard} />
          <Route path="/profile" exact component={EditProfile} />
          <Route path="/requests/:id" component={RequestDetail} />
          <Route path="/requests" exact component={RequestPage} />
        </Switch>
      </Router>
    );
  }
}

export default App;