import React, { Fragment } from 'react';

export class Countdown extends React.Component<{}, { value: number, isPaused: boolean }> {
  id: any;
  state = { value: 20, isPaused: false };

  start = () => {
    this.id = window.setInterval(() => {
      if (this.state.value <= 0) {
        clearInterval(this.id);
        return;
      }

      this.setState((state) => {
        return { value: state.value > 0 ? state.value - 1 : 0 };
      });

    }, 1000);
  };

  pause = () => {
    clearInterval(this.id);
    this.setState({ isPaused: true });
  };

  resume = () => {    
    this.setState({ isPaused: false });
    this.start();
  };

  reset = () => {
    clearInterval(this.id);
    this.setState({ value: 20 });
  };

  componentDidMount() {
    this.start();
  }

  render() {
    const { value } = this.state;
    return (
      <Fragment>
        <div className="btn-group my-2" role="group">
          <button type="button" className="btn btn-secondary" onClick={this.start}>Start</button>
          <button type="button" className="btn btn-secondary" onClick={this.state.isPaused ? this.resume : this.pause}>
            {this.state.isPaused ? 'Resume' : 'Pause'}
          </button>
          <button type="button" className="btn btn-secondary" onClick={this.reset}>Reset</button>
        </div>
        <div className="d-flex align-items-center justify-content-center bg-info rounded countdown-value">
          <h1 className="align-middle text-white font-weight-bold">{value}</h1>
        </div>
        <h3 className="mt-2">{this.state.value === 0 ? 'Blast Off!' : ''}</h3>
      </Fragment>
    );
  }
}