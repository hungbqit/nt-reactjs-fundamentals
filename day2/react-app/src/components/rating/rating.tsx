import React from 'react';

export class Rating extends React.Component<{}, { size: number, value: number }> {
    id: any;
    state = { size: 10, value: 8 };

    setValue = (index: number) => {
        this.setState({ value: index });
    };

    componentDidMount() {
    }

    render() {
        const { value } = this.state;
        const items = [];
        for (var i = 1; i <= this.state.size; i++) {
            let index = i;
            const className = index <= value ? 'btn btn-info' : 'btn btn-secondary';
            items.push(<button type="button" className={className} key={'btn-' + i} onClick={() => this.setValue(index)}>{i}</button>);
        }
        return (
            <div className="rating-container">{items}</div>
        );
    }
}