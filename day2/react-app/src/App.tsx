import React from 'react';
import './App.css';

import { Countdown } from './components/countdown/countdown';
import { Rating } from './components/rating/rating';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h2 className="mt-3">Countdown component</h2>
          <Countdown></Countdown>
          <hr className="my-5" />
          <h2 className="mt-3">Rating component</h2>
          <Rating></Rating>
        </header>
      </div>
    );
  }
}

export default App;
